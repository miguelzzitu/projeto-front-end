var myInput = document.getElementById("senha");
var conf    = document.getElementById("conf");
var letter  = document.getElementById("letter");
var number  = document.getElementById("number");
var length  = document.getElementById("length");
var bar = document.getElementById("bar");
var cont = 0;

myInput.onkeyup = function() 
{
  cont = 3;
  var upperCaseLetters = /[A-Z]/g;

  if(myInput.value.match(upperCaseLetters)) {  
    letter.classList.remove("invalid");
    letter.classList.add("valid");
  } else {
    letter.classList.remove("valid");
    letter.classList.add("invalid");
    cont --;
  } 

  var numbers = /[0-9]/g;

  if(myInput.value.match(numbers)) {  
    number.classList.remove("invalid");
    number.classList.add("valid");
  } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
    cont --;
  }

  if(myInput.value.length >= 6) {
    
    length.classList.remove("invalid");
    length.classList.add("valid");
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
    cont --;
  }

  bar.classList.remove("teste");

  if(cont == 1) {
    bar.classList.remove("bar2", "bar3");
    bar.classList.add("bar1");
  } else if(cont == 2) {
    bar.classList.remove("bar1", "bar3");
    bar.classList.add("bar2");
  } else if(cont == 3) {
    bar.classList.remove("bar1", "bar2");
    bar.classList.add("bar3");
  } else {
    bar.classList.remove("bar1", "bar2", "bar3");
    bar.classList.add("teste");
  }

}

  function validatePassword(){
  if(myInput.value != conf.value) {
    conf.setCustomValidity("Senhas diferentes");
  } else {
    conf.setCustomValidity('');
    }
}
  myInput.onchange = validatePassword;
  conf.onkeyup = validatePassword;